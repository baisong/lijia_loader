require 'sketchup.rb'
require 'extensions.rb'

module LiJia
module ChaiDan 

  # Download_Server_Address = 'http://api.chaidan.net/'.freeze
  Download_Server_Address = 'http://localhost/'.freeze

  unless file_loaded?(__FILE__)
    if Sketchup.version.to_i < 19
      UI.messagebox('立家拆单需要Sketchup 2019及以上版本', MB_OK)
    else
      ex = SketchupExtension.new('立家拆单', 'lijia_loader/lijia_loader')
      # constant_file = File.join(File.dirname(__FILE__), 'ruby', 'model', 'constant.rb')
      ruby_folder = File.join File.dirname(__FILE__), 'ruby'
      plugin_version_local = Sketchup.read_default("lijia_chaidan", "version")

      if (FileTest::exist? ruby_folder) && (!plugin_version_local.nil?)
        ex.version      =   plugin_version_local
        ex.description  =   '立家拆单 http://chaidan.net'
      else
        ex.version      =   ''
        ex.description  =   '请点击“插件”-“立家拆单”，完成自动下载'
      end
      ex.copyright    =   '柏松'
      ex.creator      =   'chaidan.net'

      Sketchup.register_extension(ex, true)
      file_loaded(__FILE__)
    end

  end
  
end
end # module LiJia
