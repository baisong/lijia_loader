## Sketchup ruby 插件自动更新功能

### 1. 简介

本插件是一个自动更新代码的插件，目前用于[立家拆单](http://chaidan.net/)插件的自动更新。

### 2. 服务器端先决条件

本功能需要服务器端提供以下两个服务的支撑。

#### 2.1 查询版本号的http服务
- 地址：Lijia::Loader::Plugin_Version_Url
- 类型：GET
- 返回内容：版本号的字符串
- 作用：ruby插件查询服务器端版本号后，比对本地版本号，如果不同，则触发下载操作

#### 2.2 下载tar.gz格式压缩包的http服务
- 地址：Lijia::Loader::Plugin_Download_Url
- 类型：GET
- 返回内容：要更新部分的tar.gz格式压缩包。ruby自带此压缩格式的支持。
- 作用：获取更新后的代码

### 3.沟通交流
讨论学习Sketchup Ruby插件开发，欢迎进入Q群：368842817